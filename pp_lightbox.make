; PP Lightbox

api = 2
core = 7.x

projects[lightbox2][subdir] = "contrib"
projects[lightbox2][version] = "1.0-beta1"

projects[download_file][subdir] = "contrib"
projects[download_file][version] = "1.1"